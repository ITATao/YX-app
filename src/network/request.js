 import axios from 'axios';
 import { Loading } from 'element-ui';
 
 /**
  * 加载动画
  */
let loadingInstance;
function showLoading(){
	loadingInstance = Loading.service({
		lock:true,
		text:'拼命加载中，请稍后!',
		background:'rgba(0,0,0,0.1)'
	});
}
function closeLoading(){
	loadingInstance.close()
}
export function request(config){
	const instance = axios.create({
		baseURL:'http://127.0.0.1:3000'
	});
	
	//请求拦截
	instance.interceptors.request.use(config=>{
		showLoading();
		return config;
	},err=>{
		return Promise.reject(err);
	})
	//响应拦截
	instance.interceptors.response.use(res=>{
		closeLoading();
		return res.data;
	},err=>{
		return Promise.reject(err);
	})
	return instance(config);
}
