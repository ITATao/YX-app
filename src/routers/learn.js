let shipin = {
	path:'/shipin',
	name:'shipin',
	meta:{title:'视频中心'},
	component:()=>import('../views/learn/shipin.vue'),
	children: [{
		path: '',
		meta: {
			title: '推荐'
		},
		component: () => import('../views/learn/shipin/recome.vue')
	},
	{
		path:'recome',
		meta:{
			title: '推荐'
		},
		component:()=>import('../views/learn/shipin/recome.vue')
	},
	{
		path:'gongyi',
		meta:{
			title: '公益汇演视频'
		},
		component:()=>import('../views/learn/shipin/gongyi.vue')
	},
	{
		path:'lizhi',
		meta:{
			title: '励志视频'
		},
		component:()=>import('../views/learn/shipin/lizhi.vue')
	},
	{
		path:'nuanxin',
		meta:{
			title: '暖心视频'
		},
		component:()=>import('../views/learn/shipin/nuanxin.vue')
	}
	]
};
let wenzahng = {
	path:'/wenzahng',
	name:'wenzahng',
	meta:{title:'文章中心'},
	component:()=>import('../views/learn/wenzahng.vue'),
	children: [{
		path: '',
		meta: {
			title: '推荐'
		},
		component: () => import('../views/learn/wengzhang/recomm.vue')
	},
	{
		path:'recomm',
		meta:{
			title: '推荐'
		},
		component:()=>import('../views/learn/wengzhang/recomm.vue')
	},
	{
		path:'xueshu',
		meta:{
			title: '学术类'
		},
		component:()=>import('../views/learn/wengzhang/xueshu.vue')
	},
	{
		path:'zhiyu',
		meta:{
			title: '治愈类'
		},
		component:()=>import('../views/learn/wengzhang/zhiyu.vue')
	}
	]
}
export {
	shipin,
	wenzahng
};