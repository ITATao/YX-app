import Vue from 'vue'
import Router from 'vue-router'
import index from '../views/index.vue'
import {anli} from './anli'
import { shipin,wenzahng } from './learn'
import { gerenziliao,fabu,shoucang,shostoryLook} from './my'

Vue.use(Router)

//解决 编程式导航报错问题  
const originalPush = Router.prototype.push;
Router.prototype.push = function push(location){  
	return originalPush.call(this, location).catch(err => err)
	};

const router = new Router({
  // mode: 'history', // 后端需要配置
  base: process.env.BASE_URL,
  routes: [
	{
	  path:'/index',
	  component:index,
	  children:[
		  {
			  path:'',
			  meta:{title:'首页'},
			  component:()=>import('../views/Home.vue')
		  },
		  {
			  path:'/Home',
			  name:'Home',
			  meta:{title:'首页'},
			  component:()=>import('../views/Home.vue')
		  },
		  anli,
		  shipin,
		  wenzahng,
		  gerenziliao,
		  fabu,
		  shoucang,
		  shostoryLook,
	  ]   
	}, 
	{
	  path: '/',
	  redirect: "/index"
	},
	{
		path:'/login',
		name:'login',
		meta:{title:'登录'},
		component:()=>import('../views/login.vue')
	},
	{
		path:'/register',
		name:'register',
		meta:{title:'注册'},
		component:()=>import('../views/register.vue')
	},
   {
	   path:'*',
	   name:'NotFound',
	   meta:{title:'页面丢失'},
	   component:()=>import('../views/notFount.vue')
   }
  ]
})

/**
 * 全局前置守卫//设置网页标题
 */
router.beforeEach((to,from,next)=>{
	document.title = to.meta.title;
	next(); //一定要调用
})

export default router;

