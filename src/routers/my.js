let gerenziliao = {
	path: '/gerenziliao',
	name: 'gerenziliao',
	component: () => import('../views/my/gerenziliao.vue'),
	children: [{
		path: '',
		meta: {
			title: '账户管理'
		},
		component: () => import('../views/my/gerenziliao/userManag.vue')
	},
	{
		path:'userManag',
		meta:{
			title: '账户管理'
		},
		component:()=>import('../views/my/gerenziliao/userManag.vue')
	},
	{
		path:'otherInfo',
		meta:{
			title: '其他信息'
		},
		component:()=>import('../views/my/gerenziliao/otherInfo.vue')
	}
	]
}
let fabu = {
	path: '/fabu',
	name: 'fabu',
	meta: {
		title: '发布'
	},
	component: () => import('../views/my/fabu.vue')
}
let shoucang = {
	path: '/shoucang',
	name: 'shoucang',
	meta: {
		title: '收藏'
	},
	component: () => import('../views/my/shoucang.vue')
}
let shostoryLook = {
	path: '/shostoryLook',
	name: 'shostoryLook',
	meta: {
		title: '评论'
	},
	component: () => import('../views/my/shostoryLook.vue')
}
export {
	gerenziliao,
	fabu,
	shoucang,
	shostoryLook
}
