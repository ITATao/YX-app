import Vue from 'vue'
import App from './App.vue'
import router from './routers/router'
import store from './store'
import ElementUI from 'element-ui'
import { request } from './network/request.js'
import 'element-ui/lib/theme-chalk/index.css'
import 'animate.css/animate.min.css'
Vue.use(ElementUI);

Vue.prototype.$request = request; //绑定到Vue原型上面
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
